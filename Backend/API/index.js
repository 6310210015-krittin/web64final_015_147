const mysql = require('mysql')
const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET


const connection = mysql.createConnection({
    host : 'localhost',
    user : 'comic_admin',
    password : 'comic_admin',
    database : 'RentComicsSystem'
})

connection.connect();

const express = require('express')
const app = express()
const port = 6000

app.listen( port, () => {
    console.log(` Now starting Rent Comics System ${port}`);
})

/* Middleware */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if(err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

/* List All Renting */
app.get("/renting_list", authenticateToken, (req, res) => {
    let user_profile = req.user
    let renter_id = user_profile.renter_id

    let query = `SELECT Renter.RenterID, Renter.Name, Renter.Surname,
                 Comics.ComicsName, RentComics.RentTime

                 FROM Renter, Comics, RentComics

                 WHERE (RentComics.RentID = Renter.RenterID) AND 
                 (RentComics.ComicsID = Comics.ComicsID);`
    
    connection.query( query, (err,rows) => {
        if(err) {
            res.json({
                    "status" : "400",
                    "message" : "Error Cannot Show List"
            })
        } else {
            res.json(rows)
        }
    });
})

/* Login For Renting Comics */
app.post("/login", (req,res) => {
    let username = req.query.username
    let password = req.query.password

    let query = `SELECT * FROM Renter
                 WHERE Username='${username}'`

    connection.query( query, (err,rows) => {
        if(err) {
            res.json({
                    "status" : "400",
                    "message" : "Error Login Failed"
            })
        } else {
            let db_password = rows[0].Password
            bcrypt.compare(password, db_password, (err, result) => {
                if(result) {
                    let payload = {
                            "username" : rows[0].Username,
                            "renter_id" : rows[0].RenterID,
                            "IsAdmin" : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                    // Sending Token
                    res.send(token)
                } else {
                    res.send("Invalid Username / Password")
                }
            })
        }
    });
})

/* Register New Renter */
app.post("/register", (req, res) => {
    let renter_name = req.query.renter_name
    let renter_surname = req.query.renter_surname
    let renter_username = req.query.renter_username
    let renter_password = req.query.renter_password

    bcrypt.hash(renter_password, SALT_ROUNDS, (err,hash) => {
        let query = `INSERT INTO Renter (Name, Surname, Username, Password, IsAdmin)
                 VALUES ('${renter_name}', '${renter_surname}',
                         '${renter_username}', '${hash}', false)`
    
        //console.log(query)

        connection.query( query, (err,rows) => {
            if(err) {
                res.json({
                        "status" : "400",
                        "message" : "Error Register"
                })
            } else {
                res.json({
                        "status" : "200",
                        "message" : "Register Success"
                })
            }
        });
    })
})

/* Renting A Comics */
app.post("/rent_comics", authenticateToken, (req, res) => {
    let user_profile = req.user
    let renter_id = user_profile.renter_id
    let comics_id = req.query.comics_id

    let query = `INSERT INTO RentComics (RenterID, ComicsID, RentTime)
                 VALUES (${renter_id}, ${comics_id}, NOW())`
    
    console.log(query)

    connection.query( query, (err,rows) => {
        if(err) {
            res.json({
                    "status" : "400",
                    "message" : "Error Renting Comics"
            })
        } else {
            res.json({
                    "status" : "200",
                    "message" : "Renting Comcis Success"
            })
        }
    });
})

/* Comics List */
app.get("/comics_list", (req,res) => {
    query = "SELECT * from Comics";
    connection.query( query, (err,rows) => {
        if(err) {
            res.json({
                    "status" : "400",
                    "message" : "Error"
            })
        } else {
            res.json(rows)
        }
    });
})

/* Add Comics */
app.post("/add_comics",authenticateToken, (req,res) => {
    let user_profile = req.user
    let renter_id = user_profile.renter_id
    let comics_name = req.query.comics_name

    if(!req.user.IsAdmin) { res.send("Unauthorized Because You're Not an Admin")}
    else {

    let query = `INSERT INTO Comics (ComicsName) VALUE ('${comics_name}')`
    //console.log(query)

    connection.query( query, (err,rows) => {
        if(err) {
            res.json({
                    "status" : "400",
                    "message" : "Error Adding Comics"
            })
        } else {
            res.json({
                    "status" : "200",
                    "message" : "Adding Comics Success"
            })
        }
    });
    }
})

/* Update Comics */
app.post("/update_comics",authenticateToken, (req,res) => {
    let user_profile = req.user
    let renter_id = user_profile.renter_id
    let comics_id = req.query.comics_id
    let comics_name = req.query.comics_name

    if(!req.user.IsAdmin) { res.send("Unauthorized Because You're Not an Admin")}
    else {

    let query = `UPDATE Comics SET
                 ComicsName='${comics_name}'
                 WHERE ComicsID=${comics_id}`
    //console.log(query)

    connection.query( query, (err,rows) => {
        if(err) {
            res.json({
                    "status" : "400",
                    "message" : "Error Updating Comics"
            })
        } else {
            res.json({
                    "status" : "200",
                    "message" : "Updating Comics Success"
            })
        }
    });
    }
})

/* Delete Comics */
app.post("/delete_comics",authenticateToken, (req,res) => {
    let user_profile = req.user
    let renter_id = user_profile.renter_id
    let comics_id = req.query.comics_id

    if(!req.user.IsAdmin) { res.send("Unauthorized Because You're Not an Admin")}
    else {

    let query = `DELETE FROM Comics WHERE ComicsID=${comics_id}`
    //console.log(query)

    connection.query( query, (err,rows) => {
        if(err) {
            res.json({
                    "status" : "400",
                    "message" : "Error Deleting Comics"
            })
        } else {
            res.json({
                    "status" : "200",
                    "message" : "Deleting Comics Success"
            })
        }
    });
    }
})

/*
query = "SELECT * from Renter";
connection.query( query, (err,rows) => {
    if(err) {
        console.log(err)
    } else {
        console.log(rows)
    }
});

connection.end();
*/