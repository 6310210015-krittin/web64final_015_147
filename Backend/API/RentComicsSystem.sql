-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 01, 2022 at 11:00 AM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `RentComicsSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `Comics`
--

CREATE TABLE `Comics` (
  `ComicsID` int(11) NOT NULL,
  `ComicsName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Comics`
--

INSERT INTO `Comics` (`ComicsID`, `ComicsName`) VALUES
(1, 'One Piece'),
(2, 'Bleach'),
(3, 'Captain America (1963)'),
(4, 'Soul Eater'),
(6, 'Jojo');

-- --------------------------------------------------------

--
-- Table structure for table `RentComics`
--

CREATE TABLE `RentComics` (
  `RentID` int(11) NOT NULL,
  `RenterID` int(11) NOT NULL,
  `ComicsID` int(11) NOT NULL,
  `RentTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `RentComics`
--

INSERT INTO `RentComics` (`RentID`, `RenterID`, `ComicsID`, `RentTime`) VALUES
(1, 1, 1, '2022-04-01 03:43:02'),
(2, 2, 4, '2022-04-01 03:43:02'),
(3, 3, 3, '2022-04-01 07:45:02'),
(4, 3, 4, '2022-04-01 07:45:33'),
(6, 4, 4, '2022-04-01 10:24:12'),
(7, 5, 2, '2022-04-01 10:35:01'),
(8, 5, 2, '2022-04-01 10:49:50'),
(9, 5, 3, '2022-04-01 10:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `Renter`
--

CREATE TABLE `Renter` (
  `RenterID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Surname` varchar(100) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Renter`
--

INSERT INTO `Renter` (`RenterID`, `Name`, `Surname`, `Username`, `Password`, `IsAdmin`) VALUES
(1, 'Anon', 'Konkorn', 'anon', 'anon', 0),
(2, 'Pramong', 'Findfish', 'pramong', 'pramong', 0),
(3, 'Eskii', 'Chichang', 'eskii', 'eskii', 0),
(4, 'Prayong', 'Zhongli', 'prayong', 'prayong', 0),
(5, 'Chadshine', 'Buybitcoin', 'chadshine', '$2b$10$Snsrg85aB6oBgdgMZmQ/Lu63HHj3ije5qx4Rj.E1QCvY13idFJh3W', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Comics`
--
ALTER TABLE `Comics`
  ADD PRIMARY KEY (`ComicsID`);

--
-- Indexes for table `RentComics`
--
ALTER TABLE `RentComics`
  ADD PRIMARY KEY (`RentID`),
  ADD KEY `RenterID` (`RenterID`),
  ADD KEY `ComicsID` (`ComicsID`);

--
-- Indexes for table `Renter`
--
ALTER TABLE `Renter`
  ADD PRIMARY KEY (`RenterID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Comics`
--
ALTER TABLE `Comics`
  MODIFY `ComicsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `RentComics`
--
ALTER TABLE `RentComics`
  MODIFY `RentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `Renter`
--
ALTER TABLE `Renter`
  MODIFY `RenterID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `RentComics`
--
ALTER TABLE `RentComics`
  ADD CONSTRAINT `ComicsID` FOREIGN KEY (`ComicsID`) REFERENCES `Comics` (`ComicsID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `RenterID` FOREIGN KEY (`RenterID`) REFERENCES `Renter` (`RenterID`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
