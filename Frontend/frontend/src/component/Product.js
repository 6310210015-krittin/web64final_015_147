
import React from 'react';
import Button from '@mui/material/Button';


import { Grid, Card } from "@mui/material";

function Product (){
    return (
        <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={3} mb={3}>
          <Card>
              <h5>เรื่อง:วันพีช ภาควาโนะคุนิ เล่มที่ 1</h5>
          <img style={{ width:200 , height: 274 }}
                            className="mr-1"
                            src="https://anime-subth.net/image/manga/one-piece/950/Upload-One-Piece-950-QWI23-By-Niceoppai.jpg"
                            alt="Onepiece" />
                <h5>รหัส: 0001</h5> <br />
             <Button fullWidth variant="contained" >เช่า</Button>
          </Card>
        </Grid>
        <Grid item xs={3}mb={3}>
        <Card>
              <h5>เรื่อง:มายฮีโร่อคาเดเมียร์ เล่ม3</h5>
          <img style={{ width:200 , height: 274 }}
                            className="mr-2"
                            src="https://pbs.twimg.com/media/EHWL0zrU4AEkJHI.jpg"
                            alt="MHA" />
                <h5>รหัส: 0002</h5> <br />
             <Button fullWidth variant="contained" >เช่า</Button>
          </Card>
        </Grid>
        <Grid item xs={3}mb={3}>
        <Card>
              <h5>เรื่อง:ดาบพิฆาตอสูร ภาคหมู่บ้านชั่งตีดาบ เล่มที่ 12</h5>
          <img style={{ width:200 , height: 250 }}
                            className="mr-3"
                            src="https://cdn-local.mebmarket.com/meb/server1/102032/Thumbnail/book_detail_large.gif?7"
                            alt="Demonslayer" />
                <h5>รหัส: 0003</h5> <br />
             <Button fullWidth variant="contained" >เช่า</Button>
          </Card>
        </Grid>
        <Grid item xs={3}mb={3}>
        <Card>
              <h5>เรื่อง:โตเกียวกูล เล่มที่ 1</h5>
          <img style={{ width:200 , height: 274 }}
                            className="mr-4"
                            src="http://f.ptcdn.info/528/040/000/o3b98abzbiTqDbsgtwb-o.jpg"
                            alt="TokyoGhu" />
                <h5>รหัส: 0004</h5> <br />
             <Button fullWidth variant="contained" >เช่า</Button>
          </Card>
        </Grid>
        <Grid item xs={3}mb={3}>
        <Card>
              <h5>เรื่อง:ดาบพิฆาตอสูร ภาครถไฟสู่นิรันดร์ เล่มที่ 8</h5>
          <img style={{ width:200 , height: 250 }}
                            className="mr-5"
                            src="https://cdn-shop.ookbee.com/Books/InterComic/2019/20190822064415/Thumbnails/Cover.jpg"
                            alt="Demonslayer" />
                <h5>รหัส: 0005</h5> <br />
             <Button fullWidth variant="contained" >เช่า</Button>
          </Card>
        </Grid>
        <Grid item xs={3} mb={3}>
        <Card>
              <h5>เรื่อง:เกิดใหม่ทั้งทีก็เป็นสไลม์ไปซะแล้ว เล่มที่ 17</h5>
          <img style={{ width:200 , height: 250 }}
                            className="mr-6"
                            src="https://www.animatejma.co.th/wp-content/uploads/2021/09/%E0%B8%AA%E0%B9%84%E0%B8%A5%E0%B8%A1%E0%B9%8C17%E0%B8%9B%E0%B8%81%E0%B8%9E%E0%B8%B4%E0%B9%80%E0%B8%A8%E0%B8%A9-822x1024.jpg"
                            alt="Slym" />
                <h5>รหัส: 0006</h5> <br />
             <Button fullWidth variant="contained" >เช่า</Button>
          </Card>
        </Grid>
        <Grid item xs={3} mb={3}>
        <Card>
              <h5>เรื่อง:นารูโตะ ภาคนินจาจอมคาถา เล่มที่ 1</h5>
          <img style={{ width:200 , height: 274 }}
                            className="mr-7"
                            src="https://cartoon.mthai.com/app/uploads/2012/07/naruto-cover001.jpg"
                            alt="Naruto" />
                <h5>รหัส: 0007</h5> <br />
             <Button fullWidth variant="contained" >เช่า</Button>
          </Card>
        </Grid>
        <Grid item xs={3} mb={3}>
        <Card>
              <h5>เรื่อง:โตเกียวรีเวนเจอร์ส เล่มที่ 1</h5>
          <img style={{ width:200 , height: 274 }}
                            className="mr-8"
                            src="https://cdn-shop.ookbee.com/Books/VIBULKIJCOMIC/2022/20220118114930135832/Thumbnails/Cover.jpg"
                            alt="TokyoRevenger" />
                <h5>รหัส: 0008</h5> <br />
             <Button fullWidth variant="contained" >เช่า</Button>
          </Card>
        </Grid>
      </Grid>
                  
    )
}

export default Product;