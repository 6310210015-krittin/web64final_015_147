import {Link} from "react-router-dom";
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { green, lightBlue } from '@mui/material/colors';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    type: 'light',
    primary: {
      main: lightBlue[100],
    },
    secondary: {
      main: green[500],
    },
  },
});

function ButtonAppBar() {
  return (
    <ThemeProvider theme={theme}>
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">
              Theme Comics Shop: 
          </Typography> 
          &nbsp;&nbsp;      
          <Link to="/">
              <Typography variant="body1">
                  SignUp 
                  </Typography>
          </Link>
        &nbsp;&nbsp;&nbsp; 
          <Link to="/login">
              <Typography variant="body1">
                  Login
                  </Typography>
          </Link>
        &nbsp;&nbsp;&nbsp;&nbsp;
          <Link to="/product">
            <Typography variant="body1">
                  Product
                  </Typography>
           </Link>
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <Link to="/about">
            <Typography variant="body1">
                  Contact
                  </Typography>
           </Link>
        </Toolbar>
      </AppBar>
    </Box>
    </ThemeProvider>
  );
}
export default ButtonAppBar;