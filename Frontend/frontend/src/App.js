import React from 'react';
import './App.css';
import { Routes, Route} from 'react-router-dom'


/* Component */
import Header from './component/Header'
import LogIn from './component/LogIn'
import Aboutuspages from './pages/Aboutuspages';
import SignUp from './component/SignUp';
import Product from './component/Product';




function App() {
  return (
   
    <div className="App">
      <Header />
      <Routes>
        <Route path='/'element={
          <SignUp /> 
        } />

        <Route path='/login'element={
          <LogIn /> 
        } />

        <Route path='/product'element={
          <Product /> 
        } />
        
        <Route path='/about'element={
          <Aboutuspages /> 
        } />

      </Routes> 
    </div> 
   
  );
}

export default App;
